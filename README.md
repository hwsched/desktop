# hwsched

An open source application that lets you enter your homework assignments and due dates, then automatically plans out when you should work on them. I made this because I couldn't seem to find an application to do this in a way that didn't involve the cloud and was open source.

Right now, this software is still in development. It will probably work, but it may still be buggy.

## Getting Started

Right now, I do not yet have an install script or packages, so you're going to have to do it manually. If you don't know how to copy and paste commands, you might wish to come back later.

### Prerequisites

All dependencies are shipped except for node, npm, and nwjs. Make sure you have them installed. There are two options for nwjs: Running with a system nwjs or running with a local nwjs. Running with a system nwjs is recommended to conserve disk space (as it can be shared with any other nwjs apps you may have), but running with a local nwjs will usually be necessary if you want to use the developer tools. (If you don't know what the developer tools are, you won't need them.)

* To use a system nwjs, you'll need to install the nwjs package from your distro, the same way you installed node and npm. (Maybe installing nw globally with npm will work too, but I haven't tested it as npm global packages are kinda annoying.)

* To use a local nwjs, you'll need to install it first, as although it would reside in node_modules, its directories are gitignored to save bandwidth and disk space. Run `npm install --include=dev` to install nwjs with dev tools locally.

### Installing

As of right now, there is no way to truly install this. That is expected to change in the future. In the meantime, the closest thing to that is downloading the source code with `git clone gitlab.com/hwsched/desktop` and running it with the instructions below.

### Running

* With system nwjs (recommended for most use)
	```
	cd /path/to/code/folder
	nw ./
	```

* With local nwjs (usually because you want the dev tools)
	```
	cd /path/to/code/folder
	npm start
	```

You may add a shell script that does this to your $PATH if you want to be able to start it in one easy to remember command.

## Built With

* [nw.js](https://nwjs.io) - A framework that allows calling node modules from the DOM to build desktop apps with HTML/CSS/JS

## Features coming soon (to 1.0.0 release)

Well, "soon" depends on how much of my time school takes up, with all of my homework and studying for AP tests. Realistically, the 1.0.0 release will probably happen in the summer.

* Keyboard control
* Performance and code quality improvements
* Better time display
* More helpful warnings
* Use icons on buttons rather than abusing characters
* Better OS integration
* Automatic install scripts so you don't have to run a bunch of commands, likely installing from the AUR

More specific info is in [TODO.txt](TODO.txt). But that file is primarily for me, so I put no effort into making it easy to follow.

## License

This project is licensed under the GPLv3 License - see the [LICENSE.md](LICENSE.md) file for details
