console.log('Starting hwsched...')

const fs = require('fs')
const path = require('path')
const os = require('os')
const envPaths = require('env-paths')('hwsched', { suffix: '' })

//Try to make required directories if they do not exist
try {
    if (!fs.existsSync(envPaths.data)) {
        fs.mkdirSync(envPaths.data, { recursive: true })
    }
    if (!fs.existsSync(envPaths.config)) {
        fs.mkdirSync(envPaths.config, { recursive: true })
    }
    if (!fs.existsSync(envPaths.cache)) {
        fs.mkdirSync(envPaths.cache, { recursive: true })
    }
} catch (error) {
    alert('Essential directories do not exist and could not be made. Error code: ' + error)
    nw.App.quit()
}

//Define where all the files go in the directories created above
global.paths = {
    assignments: path.join(envPaths.data, 'assignments.json'),
    schedule: path.join(envPaths.cache, 'schedule.json'),
    config: path.join(envPaths.config, 'config.json'),
}

const CONFIG_VERSION = 0   //This will only be incrimented if a backwards incompatible change is made to the file format.
let DEFAULT_CONFIG = {
    lastVersion: CONFIG_VERSION,
    preferredFinishTime: 'early',
    preferredMinWorkTime: 900000,
    preferredMaxWorkTime: 3600000,
    theme: 'light',
    workBlocks: [],
    servers: []
}
try {
    global.userConfig = JSON.parse(fs.readFileSync(global.paths.config))
} catch (error) {
    console.log(error)
    global.userConfig = DEFAULT_CONFIG
    fs.writeFileSync(global.paths.config, JSON.stringify(DEFAULT_CONFIG))
}

//Display an error if last modified by a new version, or do any conversion if last modified by an old version. If none is needed, open the application.
if (global.userConfig.lastVersion > CONFIG_VERSION) {
    alert('Application files were last modified by a newer, incompatible version. Application will now close.')
    nw.App.quit()
} else if (global.userConfig.lastVersion < 0) {     //Conversion from negative version up to zero
    console.log(`Converting settings from version ${global.userConfig.lastVersion} to version 0`)
    global.userConfig.lastVersion = CONFIG_VERSION
    fs.writeFileSync(global.paths.config, JSON.stringify(global.userConfig))
}

//Make sure all settings are sane
function validateWorkBlocks() {
    try {
        usedIds = []
        for (workBlock of global.userConfig.workBlocks) {
            if (usedIds.includes(workBlock.id)) { return false }
            usedIds.push(workBlock.id)
            if (
                workBlock.name == undefined ||
                parseInt(workBlock.startHour) < 0 ||
                parseInt(workBlock.startHour) > 23 ||
                isNaN(parseInt(workBlock.startHour)) ||
                parseInt(workBlock.startHour) != parseFloat(workBlock.startHour) ||
                parseInt(workBlock.startMinute) < 0 ||
                parseInt(workBlock.startMinute) > 59 ||
                isNaN(parseInt(workBlock.startMinute)) ||
                parseInt(workBlock.startMinute) != parseFloat(workBlock.startMinute) ||
                parseInt(workBlock.endHour) < 0 ||
                parseInt(workBlock.endHour) > 23 ||
                isNaN(parseInt(workBlock.endHour)) ||
                parseInt(workBlock.endHour) != parseFloat(workBlock.endHour) ||
                parseInt(workBlock.endMinute) < 0 ||
                parseInt(workBlock.endMinute) > 59 ||
                isNaN(parseInt(workBlock.endMinute)) ||
                parseInt(workBlock.endMinute) != parseFloat(workBlock.endMinute) ||
                ![true, false].includes(workBlock.days.sunday) ||
                ![true, false].includes(workBlock.days.monday) ||
                ![true, false].includes(workBlock.days.tuesday) ||
                ![true, false].includes(workBlock.days.wednesday) ||
                ![true, false].includes(workBlock.days.thursday) ||
                ![true, false].includes(workBlock.days.friday) ||
                ![true, false].includes(workBlock.days.saturday)
            ) {
                return false
            }
        }
        return true
    }
    catch {
        return false
    }
}
function validateServers() {
    //TODO: If support for servers is added, make this actually validate them rather than just returning true
    return true
}
if (!(
    ['early', 'late'].includes(global.userConfig.preferredFinishTime) &&
    typeof global.userConfig.preferredMinWorkTime == 'number' &&
    global.userConfig.preferredMinWorkTime >= 0 &&
    typeof global.userConfig.preferredMaxWorkTime == 'number' &&
    global.userConfig.preferredMaxWorkTime >= 0 &&
    global.userConfig.preferredMinWorkTime < global.userConfig.preferredMaxWorkTime &&
    ['light', 'dark', 'oled'].includes(global.userConfig.theme) &&
    validateWorkBlocks() &&
    validateServers()
)) {
    if (confirm('Configuration is invalid. Delete it and restart?')) {
        fs.unlinkSync(global.paths.config)
        nw.App.restart()
    } else {
        alert('No changes will be made. The application will now close. You will need to fix it manually and restart, or let the application delete it next time.')
        nw.App.quit()
    }
}
//Validate the other files
try {
    JSON.parse(fs.readFileSync(global.paths.assignments))
}
catch {
    if (confirm('Assignment database is invalid. Delete it and restart?')) {
        fs.writeFileSync(global.paths.assignments, '[]')
        nw.App.restart()
    } else {
        alert('No changes will be made. The application will now close. You will need to fix it manually and restart, or let the application delete it next time.')
        nw.App.quit()
    }
}
try {
    JSON.parse(fs.readFileSync(global.paths.schedule))
}
catch {
    try {
        fs.unlinkSync(global.paths.schedule)  //Don't bother to ask because it's just the cache.
    }
    catch { }
}
console.log('Setup complete. Opening main window!')
nw.Window.open('index.html')
