//I feel like the efficiency and simplicity of this code can be improved, but if we're being honest, that probably won't happen.

const fs = require('fs')
Date.prototype.toLocalISOString = function () {
    function pad(number) { return ('' + number).padStart(2, '0') }
    return `${this.getFullYear()}-${pad(this.getMonth() + 1)}-${pad(this.getDate())}T${pad(this.getHours())}:${pad(this.getMinutes())}:${pad(this.getSeconds())}`
}

//Read the user's configuration and raw list of assignments, schedule time to work on the assignments, then write that schedule to disk. Return true if there is enough time and schedule was completed, false if there was not enough time and schedule could not be completed.
exports.rebuild = function () {
    //Load assignments and remove assignments that are overdue (and won't appear anyways) to keep the database small
    let assignments = JSON.parse(fs.readFileSync(global.paths.assignments))
    assignments = assignments.filter(assignment => assignment.dueDate >= (new Date()).getTime())

    //Initialize timestamps
    firstDate = new Date()
    firstDate.setHours(0, 0, 0, 0)
    lastDate = new Date(firstDate.getTime())
    //Set the timestamps to the first and last dates to decompress work blocks to
    for (assignment of assignments) {
        if (assignment.assignedDate < firstDate) {
            firstDate = new Date(assignment.assignedDate)
        }
        if (assignment.dueDate > lastDate) {
            lastDate = new Date(assignment.dueDate)
        }
    }
    function datesInTimestampRange(first, last) {    //Get all dates between two JS timestamps, inclusive, time set to midnight
        let result = []
        currentDate = (new Date(first.getTime()))   //We don't want to modify the parameters passed by the user.
        currentDate.setHours(0, 0, 0, 0)
        while (currentDate <= last) {
            result.push(new Date(currentDate.getTime()))    //Yes, we want a copy of the object. References are a nightmare sometimes.
            currentDate.setDate(currentDate.getDate() + 1)
        }
        return result
    }
    let dateRange = datesInTimestampRange(firstDate, lastDate)
    function dayToString(day) {     //Convert the result from date.getDay() to a string representing the day of the week, lowercase
        return ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'][day]
    }

    //Decompress work blocks into an object that has every occurrence of every work block, up until one day before the due date of the last assignment, and create the list of days of the week that can be worked on. Give each work block a free time counter.
    daysWorkable = {
        sunday: false,
        monday: false,
        tuesday: false,
        wednesday: false,
        thursday: false,
        friday: false,
        saturday: false
    }
    workBlocksDecompressed = {}
    for (let currentDate of dateRange) {
        workBlocksOfDay = []
        for (workBlock of global.userConfig.workBlocks) {

            //Start time is current day and designated time of day.
            startDate = new Date(currentDate.getTime())
            startTime = new Date(currentDate.getTime())
            //Make sure the work block actually occurs on this day. If not, skip.
            if (workBlock.days[dayToString(startTime.getDay())]) {
                daysWorkable[dayToString(startTime.getDay())] = true
            } else {
                continue
            }
            startTime.setHours(workBlock.startHour, workBlock.startMinute, 0, 0)

            //End time is current day and designated time of day. If before start time, advance by one day.
            endTime = new Date(currentDate.getTime())
            endTime.setHours(workBlock.endHour, workBlock.endMinute, 0, 0)
            if (endTime < startTime) { endTime.setDate(endTime.getDate() + 1) }

            workBlockDecompressed = {
                priority: workBlock.priority,
                startDate: startDate,   //JS timestamp of date that it starts, set to midnight
                startTime: startTime,
                endTime: endTime,
                timeRemaining: (endTime - startTime)    //This will be decreased every time an assignment uses this work block.
            }
            workBlocksOfDay.push(workBlockDecompressed)
        }
        //Sort today's work blocks in the order that they should be occupied.
        //      Lower priority work blocks go before higher priority work blocks.
        //      For blocks within the same priority, blocks earlier in the day get used sooner.
        workBlocksOfDay.sort((a, b) => {
            //Return -1 if a<b, 0 if a==b, and 1 if a>b
            if (a.priority < b.priority) { return -1 }
            else if (a.priority > b.priority) { return 1 }
            else {
                if (a.startTime < b.startTime) { return -1 }
                else if (a.startTime > b.startTime) { return 1 }
                else {
                    //This code should be unreachable once I add prevention of conflicting blocks.
                    return 0
                }
            }
        })
        workBlocksDecompressed[currentDate.getTime()] = workBlocksOfDay
    }

    //Get a list of every date that can be worked on.
    datesWorkable = []
    for (date of dateRange) {
        if (daysWorkable[dayToString(date.getDay())]) {
            datesWorkable.push(date)
        }
    }

    //Sort assignments in order of due date: soonest to latest. Re-write to storage in this order to improve sorting performance later.
    assignments.sort((a, b) => {
        if (a.dueDate < b.dueDate) { return -1 }
        else if (a.dueDate > b.dueDate) { return 1 }
        else { return 0 }
    })
    fs.writeFileSync(global.paths.assignments, JSON.stringify(assignments))

    //Break assignments up into chunks: one chunk for every workable day (a day that the user has at least one work block on), from now to one day before due, unless it produces chunks smaller than the minimum acceptable working time. If it does, break it into the maximum number of possible chunks
    for (assignment of assignments) {
        assignment.chunks = []
        assignment.dates = []   //Yes, we will need this later.
        for (date of datesWorkable) {
            if (date >= assignment.assignedDate && date <= assignment.dueDate) {
                assignment.dates.push(date)
            }
        }
        //Remove the last date because it is the assignment's due date, and we want to avoid using it if possible. UNLESS THERE IS ONLY ONE DATE, because removing the only date produces an assignment that we cannot work on and also crashes the code.
        if (assignment.dates.length > 1) {
            assignment.dates = assignment.dates.slice(0, -1)
        }
        if (global.userConfig.preferredFinishTime == 'late') { assignment.dates.reverse }
        dailyWorkTime = assignment.workTime / assignment.dates.length
        dailyWorkTime = Math.ceil(dailyWorkTime / 60000) * 60000 //Round up to the nearest minute
        let totalChunks //Will be set in the next few lines, don't worry.

        //If the daily work time is too short, set it to the minimum value and adjust the number of chunks. Otherwise, set the number of chunks to be the number of dates available to work on the assignment.
        if (dailyWorkTime < global.userConfig.preferredMinWorkTime) {
            dailyWorkTime = global.userConfig.preferredMinWorkTime
            totalChunks = Math.ceil(assignment.workTime / dailyWorkTime)
        } else {
            totalChunks = assignment.dates.length
        }

        //Instead of just keeping track of time per chunk and total chunks per object, we are keeping track of each chunk individually. This is because chunks may have to be split or merged later.
        for (let i = 0; i < totalChunks; i++) {
            //Each chunk is stored as a number representing how many cos of work that chunk represents.
            assignment.chunks.push(dailyWorkTime)
        }
    }

    //Now, we're ready for the fun part.
    schedule = []
    //Remove the time from the work block, remove the chunk from the assignment, and store the chunk in the schedule.
    function assign(workBlock, assignment) {
        schedule.push({
            //Properties of the assignment, will be used by UI later
            assignmentId: assignment.id,
            assignmentName: assignment.name,
            assignmentDueDate: assignment.dueDate,

            //Assignment chunk start time = work block start time + time used. time used = total time - remaining time (before subtracting this assignment). total time = end time - start time.
            //Combines to work block start time + (end time - start time - remaining time)
            //Simplifies to end time - remaining time
            startTime: workBlock.endTime - workBlock.timeRemaining,
            endTime: (workBlock.endTime - workBlock.timeRemaining) + assignment.chunks[0]
        })
        workBlock.timeRemaining -= assignment.chunks[0]
        assignment.chunks.splice(0, 1)
    }
    //Try to fit the assignment into the work blocks of the given date. Will manipulate the chunks on the assignment object.
    function fitAssignment(assignment, date) {
        //Get today's work blocks
        workBlocksToday = workBlocksDecompressed[date.getTime()]
        //Next, we will try multiple strategies, until one eventually succeeds, and raise a warning if one doesn't.

        //Try to fit in one work block. Go through all of today's work blocks until we find one that has enough time remaining, then put the entire chunk in there. If we succeed, move on to the next assignment.
        success = false //Track if we succeeded and can move on to the next day
        for (workBlock of workBlocksToday) {
            if (workBlock.timeRemaining >= assignment.chunks[0]) {
                success = true
                assign(workBlock, assignment)
                break;
            }
        }
        if (success) {
            return true;
        }

        //If that fails, try to split into sub-chunks (up to 1 per work block) and fill them.
        for (workBlock of workBlocksToday) {
            if (workBlock.timeRemaining >= assignment.chunks[0]) {  //If there is enough time in this work block, add the remaining time to it.
                success = true
                assign(workBlock, assignment)
                break
            } else {    //If there is not enough time, fill the rest of the work block with this assignment, but DO NOT SET SUCCESS FLAG until the chunk is fully assigned.
                assignment.chunks.splice(1, 0, (assignment.chunks[0] - workBlock.timeRemaining))
                assignment.chunks[0] = workBlock.timeRemaining
                assign(workBlock, assignment)
            }
        }
        //Tell the caller if we succeeded
        return true
    }
    //For each assignment
    warning = false;    //This keeps track of if there's ever a case where assignments don't fit within the user's work blocks.
    for (let assignment of assignments) {
        //For every date that the assignment can be worked on, try to handle one chunk.
        for (let date of assignment.dates) {
            //Try to fit the assignment today. If we fail, merge whatever is left of that chunk with tomorrow's chunk
            if (fitAssignment(assignment, date)) {
                continue
            } else {
                let newChunk = assignment.chunks[0] + assignment.chunks[1]
                assignment.chunks.splice(0, 2, newChunk)
            }
        }

        //If, for some reason, there is still time left over, try to fit it in on the due date, but before the due time.
        if (assignment.chunks.length > 0) {
            date = new Date(assignment.dates[assignment.dates.length - 1])
            date.setDate(date.getDate() + 1)
            if (fitAssignment(assignment, date)) {
                continue
            } else {
                warning = true
            }
        }
    }
    //Sort the schedule. It's best to do it here because this is the part of the code that already gets called to generate the schedule. I'd rather not complicate the UI and just let it pull off in order.
    schedule.sort((a, b) => {
        if (a.startTime <= b.startTime) {
            return -1
        } else if (a.startTime >= b.startTime) {
            return 1
        } else {
            return 0
        }
    })

    //Write assignment schedule to disk
    fs.writeFileSync(global.paths.config, JSON.stringify(global.userConfig))
    fs.writeFileSync(global.paths.schedule, JSON.stringify(schedule))

    //If warning flag is set, return false to raise a low time warning in the main UI. Otherwise, return true.
    return !warning
}
